import os 
import sys 
import random 

import numpy as np 
import cv2 
import matplotlib.pylab as plt

import tensorflow as tf
from tensorflow import keras 
from sklearn.model_selection import train_test_split

seed = 2019
random.seed = seed
np.random.seed = seed 
tf.seed = seed 

class DataGen(keras.utils.Sequence):
    def __init__ (self, ids, path, input_path, mask_path, batch_size = 8, image_size = 1024):
        self.ids = ids
        self.path = path
        self.input_path = input_path
        self.mask_path = mask_path
        self.batch_size = batch_size
        self.image_size = image_size  
        self.on_epoch_end()

    def __load__ (self, id_name):
        image_path = f'{os.path.join(self.path, self.input_path, "")}luchtfoto2019_{id_name}.tif'
        mask_path = f'{os.path.join(self.path, self.mask_path, "")}Waterdeel_{id_name}.tif'
        image = cv2.imread(image_path, 1)
        image = cv2.resize(image, (self.image_size, self.image_size))

        mask = cv2.imread(mask_path, 0)
        mask = cv2.resize(mask, (self.image_size, self.image_size))
        image = image/255.0

        return image, mask 

    def __getitem__(self, index):
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        image = []
        mask = []

        for id_name in files_batch:
            _img, _mask = self.__load__(id_name)
            image.append(_img)
            mask.append(_mask)
        
        image = np.array(image) 
        mask = np.array(mask)

        return image, mask 

    def on_epoch_end(self):
        pass 

    def __len__(self):
        return int(np.ceil(len(self.ids)/float(self.batch_size)))

def get_ids():
    ids = []
    for _, _, files in os.walk(path+input_path):
        for filename in files:
            ids.append(filename[14:-4])
    return ids

image_size = 1024 
epochs = 20
batch_size = 4
path = "data/"
input_path = "input/"
mask_path = "masks/"
ids = get_ids()
train_ids, valid_ids = train_test_split(ids, test_size = 0.2)

def down_block(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(x)
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    p = keras.layers.MaxPool2D((2, 2), (2, 2))(c)
    return c, p

def up_block(x, skip, filters, kernel_size=(3, 3), padding="same", strides=1):
    us = keras.layers.UpSampling2D((2, 2))(x)
    concat = keras.layers.Concatenate()([us, skip])
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(concat)
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    return c

def bottleneck(x, filters, kernel_size=(3, 3), padding="same", strides=1):
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(x)
    c = keras.layers.Conv2D(filters, kernel_size, padding=padding, strides=strides, activation="relu")(c)
    return c

def UNet1024():
    f = [16, 32, 64, 128, 256, 512, 1024]
    inputs = keras.layers.Input((image_size, image_size, 3))
    
    p0 = inputs
    c1, p1 = down_block(p0, f[0]) #128 -> 64
    c2, p2 = down_block(p1, f[1]) #64 -> 32
    c3, p3 = down_block(p2, f[2]) #32 -> 16
    c4, p4 = down_block(p3, f[3]) #16->8
    c5, p5 = down_block(p4, f[4])
    c6, p6 = down_block(p5, f[5])

    bn = bottleneck(p6, f[6])
    
    u1 = up_block(bn, c6, f[5]) #8 -> 16
    u2 = up_block(u1, c5, f[4]) #16 -> 32
    u3 = up_block(u2, c4, f[3]) #32 -> 64
    u4 = up_block(u3, c3, f[2]) #64 -> 128
    u5 = up_block(u4, c2, f[1])
    u6 = up_block(u5, c1, f[0])

    outputs = keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(u6)
    model = keras.models.Model(inputs, outputs)
    return model

model = UNet1024()
model.compile(optimizer="adam", loss="binary_crossentropy", metrics=["acc"])

train_gen = DataGen(train_ids, path, input_path, mask_path, batch_size = batch_size, image_size = image_size)
valid_gen = DataGen(valid_ids, path, input_path, mask_path, batch_size = batch_size, image_size = image_size)

train_steps = len(train_ids)//batch_size
valid_steps = len(valid_ids)//batch_size

model.fit(train_gen, validation_data=valid_gen, steps_per_epoch=train_steps, validation_steps=valid_steps, 
                    epochs=epochs)

model.save("UNetW.h5")