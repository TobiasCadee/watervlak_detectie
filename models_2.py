from sklearn.model_selection import train_test_split

import tensorflow as tf
from tensorflow import keras

from tensorflow.python.platform.tf_logging import error
class UNet(tf.keras.Model):
    def __init__(self, size, image_size, dropout = 0.0):
        self.dropout= dropout
        if size == 1024:
            inputs, outputs = self.UNet1024(image_size)
            super(UNet, self).__init__(inputs, outputs)
        elif size == 512:
            inputs, outputs = self.UNet512(image_size)
            super(UNet, self).__init__(inputs, outputs)
        elif size == 256:
            inputs, outputs = self.UNet256(image_size)
            super(UNet, self).__init__(inputs, outputs)


    def down_block(self, x, filters, kernel_size=(3, 3), padding = 'same'):
        c = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(x)
        c2 = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(x)
        mp = keras.layers.MaxPool2D((2, 2), (2, 2))(c2)
        dp = keras.layers.Dropout(self.dropout)(mp)
        return c, dp

    def up_block(self, x, skip, filters, kernel_size=(3, 3), padding = 'same'):
        us = keras.layers.UpSampling2D((2, 2))(x)
        concat = keras.layers.Concatenate()([us, skip])
        dropout = keras.layers.Dropout(self.dropout)(concat)
        c = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(dropout)
        c2 = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(c)
        return c2

    def bottleneck(self, x, filters, kernel_size=(3, 3), padding = 'same'):
        c = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(x)
        c2 = keras.layers.Conv2D(filters, kernel_size, padding=padding, activation = "relu")(x)
        return c2

    def UNet1024(self, image_size):
        f = [16, 32, 64, 128, 256, 512, 1024]
        inputs = keras.layers.Input((image_size, image_size, 3))

        p0 = inputs
        c1, p1 = self.down_block(p0, f[0]) #128 -> 64
        c2, p2 = self.down_block(p1, f[1]) #64 -> 32
        c3, p3 = self.down_block(p2, f[2]) #32 -> 16
        c4, p4 = self.down_block(p3, f[3]) #16->8
        c5, p5 = self.down_block(p4, f[4])
        c6, p6 = self.down_block(p5, f[5])

        bn = self.bottleneck(p6, f[6])
        
        u1 = self.up_block(bn, c6, f[5]) #8 -> 16
        u2 = self.up_block(u1, c5, f[4]) #16 -> 32
        u3 = self.up_block(u2, c4, f[3]) #32 -> 64
        u4 = self.up_block(u3, c3, f[2]) #64 -> 128
        u5 = self.up_block(u4, c2, f[1])
        u6 = self.up_block(u5, c1, f[0])

        outputs = keras.layers.Conv2D(1, (1, 1), padding="same", activation="sigmoid")(u6)
        return inputs, outputs

    def UNet512(self, image_size):
        f = [16, 32, 64, 128, 256, 512]
        inputs = keras.layers.Input((image_size, image_size, 3))
        if self.dropout > 0.4:
            initial_dropout = 0.2
        else:
            initial_dropout = self.dropout * 0.5 
        p0 = inputs
        c1, p1 = self.down_block(p0, f[0], dropout = initial_dropout) #128 -> 64
        c2, p2 = self.down_block(p1, f[1], dropout = self.dropout) #64 -> 32
        c3, p3 = self.down_block(p2, f[2], dropout = self.dropout) #32 -> 16
        c4, p4 = self.down_block(p3, f[3], dropout = self.dropout) #16->8
        c5, p5 = self.down_block(p4, f[4], dropout = self.dropout)

        bn = self.bottleneck(p5, f[5])
        
        u1 = self.up_block(bn, c5, f[4]) #8 -> 16
        u2 = self.up_block(u1, c4, f[3]) #16 -> 32
        u3 = self.up_block(u2, c3, f[2]) #32 -> 64
        u4 = self.up_block(u3, c2, f[1]) #64 -> 128
        u5 = self.up_block(u4, c1, f[0])

        outputs = keras.layers.Conv2D(1, 1, 1, activation="sigmoid")(u5)
        return inputs, outputs

    def UNet256(self, image_size):
        f = [16, 32, 64, 128, 256]
        inputs = keras.layers.Input((image_size, image_size, 3))
        p0 = inputs
        c1, p1 = self.down_block(p0, f[0]) #128 -> 64
        c2, p2 = self.down_block(p1, f[1]) #64 -> 32
        c3, p3 = self.down_block(p2, f[2]) #32 -> 16
        c4, p4 = self.down_block(p3, f[3]) #16->8

        bn = self.bottleneck(p4, f[4])
        
        u1 = self.up_block(bn, c4, f[3]) #8 -> 16
        u2 = self.up_block(u1, c3, f[2]) #16 -> 32
        u3 = self.up_block(u2, c2, f[1]) #32 -> 64
        u4 = self.up_block(u3, c1, f[0]) #64 -> 128

        outputs = keras.layers.Conv2D(1, (1,1), activation="sigmoid")(u4)
        return inputs, outputs