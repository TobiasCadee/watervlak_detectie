from tensorflow import keras
import os
import cv2
import numpy as np

class DataGen(keras.utils.Sequence):
    def __init__ (self, ids, path, input_path, mask_path, batch_size = 16, image_size = 512):
        self.ids = ids
        self.path = path
        self.input_path = input_path
        self.mask_path = mask_path
        self.batch_size = batch_size
        self.image_size = image_size  
        self.on_epoch_end()

    def __load__ (self, id_name):
        image_path = f'{os.path.join(self.path, self.input_path, "")}luchtfoto2019_{id_name}.tif'
        mask_path = f'{os.path.join(self.path, self.mask_path, "")}Waterdeel_{id_name}.tif'
        image = cv2.imread(image_path, 1)

        mask = cv2.imread(mask_path, 0)
        mask = mask.reshape(self.image_size, self.image_size, 1)
        image = image/255.0

        return image, mask 

    def __getitem__(self, index):
        if(index+1)*self.batch_size > len(self.ids):
            self.batch_size = len(self.ids) - index*self.batch_size
        files_batch = self.ids[index*self.batch_size : (index+1)*self.batch_size]

        image = []
        mask = []

        for id_name in files_batch:
            _img, _mask = self.__load__(id_name)
            image.append(_img)
            mask.append(_mask)
        
        image = np.float32(np.array(image))
        mask = np.float32(np.array(mask))

        return image, mask 

    def on_epoch_end(self):
        pass 

    def __len__(self):
        return int(np.ceil(len(self.ids)/float(self.batch_size)))